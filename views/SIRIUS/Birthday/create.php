<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;


if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();


include("header.php");
?>
<div class="text-center">
    <h1 style="color:#2f2f2f">Birth Date Entry</h1>
    <h4 class="massage"><?php echo $msg;?></h4>
    <div class="form-group Form">
        <form action="store.php" method="post">
            <div>
                <h3>Enter Your Name: </h3>
                <input type="text" name="userName">
            </div>

            <div>
                <h3>Enter Your Birth Date: </h3>
                <input type="date" name="birthDate">
            </div>
            <br><br>
            <input type="submit" class="btn btn-primary" value="CREATE">
        </form>
    </div>
</div>
<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    jQuery(function($){
        $('.massage').fadeOut(500);
        $('.massage').fadeIn(500);
        $('.massage').fadeOut(500);
        $('.massage').fadeIn(500);
        $('.massage').fadeOut(500);
        $('.massage').fadeIn(500);
        $('.massage').fadeOut(500);
    })
</script>
<?php include("footer.php");?>