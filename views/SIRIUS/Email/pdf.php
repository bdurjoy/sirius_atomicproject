
<?php
include_once ('../../../vendor/autoload.php');
use App\Email\Email;

$obj= new Email();
$recordSet=$obj->index();
//var_dump($allData);
$trs="";
$sl=0;

foreach($recordSet as $row) {
    $id =  $row->id;
    $userName = $row->user_name;
    $emailAdd =$row->email_add;

    $sl++;
    $trs .= "<tr>";
    $trs .= "<td width='80'> $sl</td>";
    $trs .= "<td width='80'> $id </td>";
    $trs .= "<td width='250'> $userName </td>";
    $trs .= "<td width='250'> $emailAdd </td>";

    $trs .= "</tr>";
}

$html= <<<BITM
<head>
    <style>
        td{
            border: 2px solid grey;
        }
        table{border: 4px solid black;
        }

    </style>
    </head>
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Person Name</th>
                    <th align='left' >Email Address</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>
            </div>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('Email.pdf', 'D');