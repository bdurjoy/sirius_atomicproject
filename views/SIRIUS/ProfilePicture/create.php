<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            width:100%;
            padding:0px;
            margin:0px;
        }
        .bg-1 {
            background-color: #F8F8F8;
            color: black;
        }
       /* p {font-size: 16px;}
        .margin {margin-bottom: 45px;}
        .bg-1 {
            background-color: #1abc9c;
            color: #ffffff;
        }
        .bg-2 {
            background-color: #474e5d;
            color: #ffffff;
        }
        .bg-3 {
            background-color: #ffffff;
            color: #555555;
        }
        .bg-4 {
            background-color: #2f2f2f;
            color: #fff;
        }
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: #1abc9c;
        }
        .checkBox{
            margin: 0 40%;
        }*/
        #file{
            margin-top: 20px;
            margin-left: 40%;
        }
        </style>
</head>
<body class="bg-4">
<div class="container bg-1 text-center">
    <h1>Profile Picture - Upload Form</h1>
    <?php echo "<div class= 'massage'> $msg </div>";?>
<form class="form-group" name="file" action="store.php" method="post" enctype="multipart/form-data">
    <h4>Enter Name:</h4>
        <input type="text" name="userName">
    <br>
        <input type="file" id="file" name="file">
    <br>
    <input type="submit" class="btn btn-primary" value="Upload">
    <a href='index.php' class='btn btn-primary'>Back</a>
    
</form>
    </div>
<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    $(document).ready(function () {
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
    })
</script>
</body>
</html>