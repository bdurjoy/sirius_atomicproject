<?php


require_once("../../../vendor/autoload.php");
$objProfile = new \App\ProfilePicture\ProfilePicture();
$objProfile->setData($_GET);
$oneData = $objProfile->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            width:100%;
            padding:0px;
            margin:0px;
        }
       /* p {font-size: 16px;}
        .margin {margin-bottom: 45px;}*/
        .bg-1 {
            background-color: #F8F8F8;
            color: black;
        }
      /*  .bg-2 {
            background-color: #474e5d;
            color: #ffffff;
        }
        .bg-3 {
            background-color: #ffffff;
            color: #555555;
        }
        .bg-4 {
            background-color: #2f2f2f;
            color: #fff;
        }
        .container-fluid {
            padding-top: 70px;
            padding-bottom: 70px;
        }
        .navbar {
            padding-top: 15px;
            padding-bottom: 15px;
            border: 0;
            border-radius: 0;
            margin-bottom: 0;
            font-size: 12px;
            letter-spacing: 5px;
        }
        .navbar-nav  li a:hover {
            color: #1abc9c !important;
        }

        input{
            color: #1abc9c;
        }
        .checkBox{
            margin: 0 40%;
        }
        img{
            max-width: 600px;
            max-height: 400px;
        }*/
    </style>
</head>
<body class="bg-4">
<div class="">
<?php

echo "
<div class='container'><br/>
<div align='center'>
 <img title='Profile Picture' width='200' height='200' src='images/$oneData->profile_pic_link'> <br> <br>
        <h5><strong>Profile Picture: </strong> <a href='#'>$oneData->user_name </a> </h5>
        <a href='index2.php'>Home </a>
</div>
<table  class='table table-striped table-bordered text-center'>
    <tr>
        <td>ID: </td>
        <td>$oneData->id</td>
        <td></td>
    </tr>
    <tr>
        <td>Name: </td>
        <td>$oneData->user_name</td>
        <td><a href='index.php' class='btn btn-info'>Back To Active List</a></td>
    </tr>

</table>
</div>





";
?>
</div>
<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    $(document).ready(function () {
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
    })
</script>
    </body>
</html>