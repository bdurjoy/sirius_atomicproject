<?php
include_once ('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;

$obj= new ProfilePicture();
 $recordSet=$obj->index();
 //var_dump($allData);
$trs="";
$sl=0;

    foreach($recordSet as $row) {
        $id =  $row->id;
        $userName = $row->user_name;
        $profilepic =$row->profile_pic_link;

        $sl++;
        $trs .= "<tr>";
        $trs .= "<td width='50px'> $sl</td>";
        $trs .= "<td width='50px'> $id </td>";
        $trs .= "<td width='150px'> $userName </td>";
        $trs .= "<td width='150px'><img height='130px' width='130px' src='images/$profilepic'/> </td>";

        $trs .= "</tr>";
    }

$html= <<<BITM
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th align='left'>Serial</th>
                    <th align='left' >ID</th>
                    <th align='left' >Person Name</th>
                    <th align='left' >Profile Picture</th>

              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>


BITM;


// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('profilepicture.pdf', 'D');