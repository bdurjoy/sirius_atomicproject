<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;
$objHobby = new \App\Hobby\Hobby();
$objHobby->setData($_GET);
$oneData = $objHobby->view();


if(!isset($_SESSION)){
    session_start();
}

$msg = Message::getMessage();



?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Title - Active List</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/bootstrap.min.css">
    <style>
        body {
            width:100%;
            padding:0px;
            margin:0px;
        }
        /* p {font-size: 16px;}
         .margin {margin-bottom: 45px;}*/
        .bg-1 {
            background-color: #F8F8F8;
            color: grey;
        }
        /* .bg-2 {
             background-color: #474e5d;
             color: #ffffff;
         }
         .bg-3 {
             background-color: #ffffff;
             color: #555555;
         }
         .bg-4 {
             background-color: #2f2f2f;
             color: #fff;
         }
         .container-fluid {
             padding-top: 70px;
             padding-bottom: 70px;
         }
         .navbar {
             padding-top: 15px;
             padding-bottom: 15px;
             border: 0;
             border-radius: 0;
             margin-bottom: 0;
             font-size: 12px;
             letter-spacing: 5px;
         }
         .navbar-nav  li a:hover {
             color: #1abc9c !important;
         }

         input{
             color: black;
         }*/
    </style>
</head>
<body><div class="container bg-1 text-center">
    <h1 style="color:#2f2f2f">Hobby Information Update</h1>
    <h4 class="massage"><?php echo $msg;?></h4>
    <div class="form-group Form">
        <form action="update.php" method="post">
            <div>
                
                <h3>Person Name:</h3>
                <input type="text" name="userName" value="<?php echo $oneData->user_name?>">
            </div>

            <div class="checkBox">
                <h3>Please Select Your Hobby: </h3>
                <?php
                $hobbyArray=explode(" ",$oneData->hobbies);
               // var_dump($hobbyArray);
                ?>

                <input type="checkbox" <?php if(in_array("gardening",$hobbyArray)) echo "checked"; ?> name="hobby1"  value="gardening">Gardening<br>

                <input type="checkbox" <?php if(in_array("music",$hobbyArray)) echo "checked"; ?> name="hobby2" value="music">Music<br>

                <input type="checkbox" <?php if(in_array("programming",$hobbyArray)) echo "checked"; ?> name="hobby3" value="programming">Pragramming<br>
                <input type="checkbox" <?php if(in_array("reading",$hobbyArray)) echo "checked"; ?> name="hobby4" value="reading">Reading Book<br>
                <input type="checkbox" <?php if(in_array("sports",$hobbyArray)) echo "checked"; ?> name="hobby5" value="sports">Sports<br>
                <input type="checkbox" <?php if(in_array("drawing",$hobbyArray)) echo "checked"; ?> name="hobby6" value="drawing">Drawing<br>

            </div>
            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
            <input type="submit" class="btn btn-primary" value="Update">
        </form>
        </div>
    </div>

<script src="../../../resources/js/jquery.js"></script>
<script src="../../../resources/js/jquery-3.1.1.js"></script>
<script>
    $(document).ready(function () {
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
        $('.massage').fadeIn(450);
        $('.massage').fadeOut(450);
    })
</script>
</body>
</html>